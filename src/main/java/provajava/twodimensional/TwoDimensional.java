package provajava.twodimensional;

import java.util.Scanner;

public class TwoDimensional {

    private int[][] tdArray = new int[][]{{50, 60, 55, 67, 70}, {62, 65, 70, 70, 81}, {72, 66, 77, 80, 69}};
    private int x;
    private int y;

    public int[][] newArrayTwoDimensional(){

        int[][] result = new int[5][3];

        for (int i = 0; i <= this.x; i++){
            for (int j =0; j <= this.y; j++){
                result[i][j] = ((tdArray[i][j+1] + tdArray[i+1][j+1])/2);
            }
        }

        return result;

    }

    public static void main(String args[]){

        TwoDimensional td = new TwoDimensional();

        //Scanner scan = new Scanner(System.in);

        System.out.print(td.newArrayTwoDimensional().toString());

    }

    public int[][] getTdArray() {
        return tdArray;
    }

    public void setTdArray(int[][] tdArray) {
        this.tdArray = tdArray;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
