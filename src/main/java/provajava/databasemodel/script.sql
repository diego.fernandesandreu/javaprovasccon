

-- -----------------------------------------------------
-- Table `assetos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `assetos` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `assetos` (
  `id_assetos` INT NOT NULL,
  `numero` INT NOT NULL,
  `horario_id_horario` INT NOT NULL,
  `horario_cinema_id_cinema` INT NOT NULL,
  `horario_cinema_lista_filmes_id_lista_filmes` INT NOT NULL,
  `horario_cinema_lista_filmes_cidade_id_cidade` INT NOT NULL,
  PRIMARY KEY (`id_assetos`, `horario_id_horario`, `horario_cinema_id_cinema`, `horario_cinema_lista_filmes_id_lista_filmes`, `horario_cinema_lista_filmes_cidade_id_cidade`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_assetos_horario1_idx` ON `assetos` (`horario_id_horario` ASC, `horario_cinema_id_cinema` ASC, `horario_cinema_lista_filmes_id_lista_filmes` ASC, `horario_cinema_lista_filmes_cidade_id_cidade` ASC) VISIBLE;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cidade` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cidade` (
  `id_cidade` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_cidade`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `cinema`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `cinema` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `cinema` (
  `id_cinema` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `lista_filmes_id_lista_filmes` INT NOT NULL,
  `lista_filmes_cidade_id_cidade` INT NOT NULL,
  PRIMARY KEY (`id_cinema`, `lista_filmes_id_lista_filmes`, `lista_filmes_cidade_id_cidade`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_cinema_lista_filmes1_idx` ON `cinema` (`lista_filmes_id_lista_filmes` ASC, `lista_filmes_cidade_id_cidade` ASC) VISIBLE;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `horario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `horario` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `horario` (
  `id_horario` INT NOT NULL,
  `horario` DATETIME NOT NULL,
  `cinema_id_cinema` INT NOT NULL,
  `cinema_lista_filmes_id_lista_filmes` INT NOT NULL,
  `cinema_lista_filmes_cidade_id_cidade` INT NOT NULL,
  PRIMARY KEY (`id_horario`, `cinema_id_cinema`, `cinema_lista_filmes_id_lista_filmes`, `cinema_lista_filmes_cidade_id_cidade`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_horario_cinema1_idx` ON `horario` (`cinema_id_cinema` ASC, `cinema_lista_filmes_id_lista_filmes` ASC, `cinema_lista_filmes_cidade_id_cidade` ASC) VISIBLE;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `lista_filmes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lista_filmes` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `lista_filmes` (
  `id_lista_filmes` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `cidade_id_cidade` INT NOT NULL,
  PRIMARY KEY (`id_lista_filmes`, `cidade_id_cidade`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE INDEX `fk_lista_filmes_cidade_idx` ON `lista_filmes` (`cidade_id_cidade` ASC) VISIBLE;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
