package provajava.complexnumbers;

public class ComplexNumber {

    private double real1;
    private double real2;
    private double imaginary1;
    private double imaginary2;

    public ComplexNumber() {
        this.real1 = 0d;
        this.real2 = 0d;
        this.imaginary1 = 0d;
        this.imaginary2 = 0d;
    }

    public ComplexNumber( double real1, double real2, double imaginary1, double imaginary2 )
    {
        this.real1 = real1;
        this.real2 = real2;
        this.imaginary1 = imaginary1;
        this.imaginary2 = imaginary2;
    }

    public static void sumComplexNumbers(ComplexNumber complexNumber){

        double real = complexNumber.getReal1() + complexNumber.getImaginary2();
        double imaginary = complexNumber.getImaginary1() + complexNumber.getImaginary2();
        System.out.printf("(%.1f + %.1fi) + (%.1f + %.1fi) = (%.1f + %.1fi)\n",
                complexNumber.getReal1(), complexNumber.getImaginary1(), complexNumber.getImaginary2(), complexNumber.getImaginary2(), real, imaginary);

    }

    public static void thirdComplexNumber (ComplexNumber complexNumber){
        sumComplexNumbers(complexNumber);
    }

    public double getReal1() {
        return real1;
    }

    public void setReal1(double real1) {
        this.real1 = real1;
    }

    public double getReal2() {
        return real2;
    }

    public void setReal2(double real2) {
        this.real2 = real2;
    }

    public double getImaginary1() {
        return imaginary1;
    }

    public void setImaginary1(double imaginary1) {
        this.imaginary1 = imaginary1;
    }

    public double getImaginary2() {
        return imaginary2;
    }

    public void setImaginary2(double imaginary2) {
        this.imaginary2 = imaginary2;
    }

    public static void main(String args[] ){

        ComplexNumber cn = new ComplexNumber(5, 10, 3, 2);
        cn.sumComplexNumbers(cn);

        ComplexNumber.thirdComplexNumber(new ComplexNumber(26, 5, 1, 9));

    }

}
