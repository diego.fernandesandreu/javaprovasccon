package provajava.countnames;

import java.text.Normalizer;
import java.util.*;

public class CountNames {

    private List<String> names;
    private Map<String, Integer> count;

    public CountNames(){

        this.names = new ArrayList<>();
        this.names.add("Pedro");
        this.names.add("João");
        this.names.add("Maria");
        this.names.add("JOAO");
        this.names.add("Alberto");
        this.names.add("João");
        this.names.add("MARiA");
        this.names.add("PEdrO");
        this.names.add("ALBERTO");

    }

    public Map<String, Integer> countNames(){

        this.count = new HashMap<>();

        Collections.sort(names);

        for (String name : this.names){

            name = unaccent(name).toUpperCase();

            if (!this.count.containsKey(name)){
                this.count.put(name, 0);
            }
            this.count.put(name, this.count.get(name) + 1);
        }

        return this.count;

    }

    private static String unaccent(String src) {
        return Normalizer
                .normalize(src, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }

    public static void main(String args[]){

        CountNames cn = new CountNames();

        System.out.print(cn.countNames().toString());

    }


}
